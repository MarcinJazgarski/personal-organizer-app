package com.sda.main;

import com.sda.calories.AbsorbedCaloriesDTO;
import com.sda.settings.ConfigDTO;
import com.sda.settings.ConfigDTOoperations;
import com.sda.weather.WeatherDTO;
import com.sda.weather.WeatherLoader;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Controller implements Initializable {

    private Logger logger = LogManager.getLogger(Controller.class);
    private double caloriesLimit;
    private double absorbedCalories;
    private final AbsorbedCaloriesDTO absorbedCaloriesDTO = new AbsorbedCaloriesDTO();
    private final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    private WeatherDTO weatherDTO = null;

    @FXML
    Parent mainPane;
    @FXML
    ScrollPane mainScrollPane;

    //welcome
    @FXML
    Label usernameLabel;

    //com.sda.notes
    @FXML
    Label visibleNoteLabel;

    //calories
    @FXML
    PieChart caloriesPieChart;
    @FXML
    Label caloriesAbsorbedLabel;
    @FXML
    TextField newCaloriesTextField;
    @FXML
    Label addCaloriesLabel;

    //weather
    @FXML
    Label wLocalizationLabel;
    @FXML
    Label wTheTempLabel;
    @FXML
    Label wMinTempLabel;
    @FXML
    Label wMaxTempLabel;
    @FXML
    ImageView wImageView;

    //events
    @FXML
    ListView<String> eventsTodayListView;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        loadData();

        //-----CALORIES - Scheduled executor (checks 1 time per minute if it is a new day, if yes then reset today's calories intake to zero-----
        ScheduledExecutorService caloriesUpdater = Executors.newSingleThreadScheduledExecutor();
        caloriesUpdater.scheduleAtFixedRate(() -> {
            Platform.runLater(() -> {
                String dt = absorbedCaloriesDTO.loadDateOfUpdateFromPropertiesFile(); //load date of last calories update from properties field
                LocalDate dtUpdate = null;
                if (dt != null) {
                    dtUpdate = LocalDate.parse(dt, dtf);
                }
                LocalDate dtCurrent = LocalDate.now(); //get current system date
                if (dtUpdate != null && dtCurrent.isAfter(dtUpdate)) { //if current date is a new date then set calories to zero -> save to properties file and update pie chart
                    absorbedCaloriesDTO.saveToCaloriesPropertiesFile(0d);
                    loadData();
                }
            });
        }, 0, 1, TimeUnit.MINUTES);


        //-----WEATHER - Scheduled executor (weather updater)-----
        ScheduledExecutorService weatherUpdater = Executors.newSingleThreadScheduledExecutor();
        weatherUpdater.scheduleAtFixedRate(() -> {
            Platform.runLater(() -> {
                LocalDateTime dt = LocalDateTime.now();
                if (dt.isAfter(weatherDTO.getTimeStampLastWeatherDTOObjectUpdate().plusMinutes(30))) { //checks if 30 mins elapsed from last weather update
                    loadData();
                    System.out.println(weatherDTO.getTimeStampLastWeatherDTOObjectUpdate().toString());
                }
            });
        }, 0, 10, TimeUnit.MINUTES); //the condition check is performed every 10 minutes



        mainScrollPane.prefWidthProperty().bind(((Pane) mainPane).widthProperty());
        mainScrollPane.prefHeightProperty().bind(((Pane) mainPane).heightProperty());
        eventsTodayListView.getItems().add("Mary's birthday.");
        eventsTodayListView.getItems().add("Annual meeting.");
        caloriesPieChart.setStartAngle(90);
    }

    public void loadSettings(MouseEvent mouseEvent) {
        URL resource = getClass().getClassLoader().getResource("settings.fxml");
        if (resource != null) {
            try {
                Pane settingsPane = FXMLLoader.load(resource);
                Stage stage = (Stage) mainPane.getScene().getWindow();
                stage.setScene(new Scene(settingsPane, 1040, 1000));
                stage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void loadNotes (MouseEvent mouseEvent){
        URL resource = getClass().getClassLoader().getResource("notes.fxml");
        if(resource != null){
            try{
                Pane notesPane = FXMLLoader.load(resource);
                Stage stage = (Stage) mainPane.getScene().getWindow();
                stage.setScene(new Scene(notesPane,1040, 1000));
                stage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    private void loadData() {
        //-----USER PROPERTIES-----
        //load saved user settings from properties file
        ConfigDTOoperations configDTOoperations = new ConfigDTOoperations();
        configDTOoperations.loadConfigProperties();
        ConfigDTO configDTO = configDTOoperations.loadConfigProperties();
        usernameLabel.setText(configDTO.getUserName());

        //-----CALORIES-----
        Double d = absorbedCaloriesDTO.loadFromCaloriesPropertiesFile(); //load saved calories properties file
        absorbedCalories = (d == null ? 0 : d);
        caloriesLimit = configDTO.getCaloriesUserLimit();  //initiate values of 2 pie chart pieces
        ObservableList<PieChart.Data> caloriesData = createPieChartDataSet(absorbedCalories, caloriesLimit); //set up data for 2 pie chart pieces
        this.caloriesPieChart.setData(caloriesData); //pass data to FMXL pie chart component
        applyCustomColorSequenceToPieChart(caloriesData, "RED", "WHITE");
        this.caloriesAbsorbedLabel.setText(Double.valueOf(absorbedCalories).toString()); //pass value of absorbed to display as text label on the piechart

        //-----WEATHER-----
        WeatherLoader weatherLoader = new WeatherLoader();
        weatherDTO = weatherLoader.loadWeather(configDTO.getLocalization());
        weatherDTO.setTimeStampLastWeatherDTOObjectUpdate(LocalDateTime.now());
        wTheTempLabel.setText(weatherDTO.getTheTemp().toString() + "C");
        wMinTempLabel.setText("min " + weatherDTO.getMinTemp().toString() + "C");
        wMaxTempLabel.setText("max " + weatherDTO.getMaxTemp().toString() + "C");
                wLocalizationLabel.setText(weatherDTO.getCity());

        wImageView.setImage(new Image("icons/weather-" + weatherDTO.getWeatherState().toString().toLowerCase() + ".png"));


    }

    public ObservableList<PieChart.Data> createPieChartDataSet(double absorbedCalories, double caloriesLimit) {
        ObservableList<PieChart.Data> caloriesData;
        if (absorbedCalories < caloriesLimit) {
            caloriesData = FXCollections.observableArrayList(
                    new PieChart.Data("Absorbed", absorbedCalories),
                    new PieChart.Data("Pending", caloriesLimit - absorbedCalories));
        } else
            caloriesData = FXCollections.observableArrayList(
                    new PieChart.Data("Absorbed", absorbedCalories),
                    new PieChart.Data("Pending", 0));
        return caloriesData;
    }

    public void updateAbsorbedCalories(MouseEvent mouseEvent) {
        absorbedCalories += Double.valueOf(newCaloriesTextField.getText());
        ObservableList<PieChart.Data> caloriesData = createPieChartDataSet(absorbedCalories, caloriesLimit); //set up data for 2 pie chart pieces
        this.caloriesPieChart.setData(caloriesData); //pass data to FMXL pie chart component
        applyCustomColorSequenceToPieChart(caloriesData, "RED", "WHITE");
        this.caloriesAbsorbedLabel.setText(String.valueOf(absorbedCalories)); //pass value of absorbed to display as text label on the piechart
        absorbedCaloriesDTO.saveToCaloriesPropertiesFile(absorbedCalories); //save absorbed calories to properties file
    }


    private void applyCustomColorSequenceToPieChart(ObservableList<PieChart.Data> pieChartData, String... pieColors) {
        int i = 0;
        for (PieChart.Data data : pieChartData) {
            data.getNode().setStyle("-fx-pie-color: " + pieColors[i % pieColors.length] + ";");
            i++;
        }
    }

}
