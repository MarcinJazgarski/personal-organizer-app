package com.sda.notes;

import com.sda.settings.SettingsController;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

public class NotesController implements Initializable {

    private Logger logger = LogManager.getLogger(SettingsController.class);
    private List<Note> noteArrayList = new ArrayList<>();

    @FXML
    Pane notesPane;

    @FXML
    ScrollPane notesScrollPane;

    @FXML
    FlowPane notesFlowPane;

    @FXML
    TextArea newNoteTextArea;

    @FXML
    Label addNoteLabel;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        notesScrollPane.prefWidthProperty().bind(notesPane.widthProperty());
        notesScrollPane.prefHeightProperty().bind(notesPane.heightProperty());
        loadNotes();
    }

    public void loadNotes() {
        //ładowanie kafelków z notatkami
        for (Note n : noteArrayList) {
            createNoteTile(n);
        }
    }

    public void createNoteTile(Note n) {

        Pane notePane = new Pane();  // notePane: Notes Tile
        notePane.setPrefSize(360, 240);
        notePane.setMaxSize(360, 240);
        notePane.setMinSize(360, 240);
        notePane.setStyle("-fx-background-color: TEAL ");


        ImageView deleteIcon = new ImageView(new Image("icons/delete.png"));  // deleteIcon: delete Image
        deleteIcon.setFitWidth(20);
        deleteIcon.setFitHeight(20);
        deleteIcon.setLayoutX(320);
        deleteIcon.setLayoutY(10);
        deleteIcon.setCursor(Cursor.HAND);
        deleteIcon.setOnMouseClicked(event -> {
            int decision = newDeleteAlert();
            if (decision == 1) {
                notesFlowPane.getChildren().remove(notePane);
            }
        });

        TextArea textArea = new TextArea();
        textArea.setLayoutX(20);
        textArea.setLayoutY(35);
        textArea.setMinSize(320, 190);
        textArea.setMaxSize(320, 190);
        textArea.setStyle(("-fx-control-inner-background: TEAL; -fx-font-size: 16"));
        textArea.setText(n.getText());
        textArea.setEditable(false);

        ImageView editIcon = new ImageView(new Image("icons/edit.png"));  // editIcon: Edit Image
        editIcon.setFitWidth(20);
        editIcon.setFitHeight(20);
        editIcon.setLayoutX(290);
        editIcon.setLayoutY(10);
        editIcon.setCursor(Cursor.HAND);
        editIcon.setOnMouseClicked(event -> {
            textArea.setEditable(true);

            ImageView okIcon = new ImageView(new Image("icons/ok.png"));  // okIcon: Ok Image
            okIcon.setFitWidth(20);
            okIcon.setFitHeight(20);
            okIcon.setLayoutX(260);
            okIcon.setLayoutY(10);
            okIcon.setCursor(Cursor.HAND);
            notePane.getChildren().add(okIcon);
            okIcon.setOnMouseClicked(event1 -> {
                textArea.setEditable(false);
                notePane.getChildren().remove(okIcon);
            });
        });

        notePane.getChildren().add(editIcon);
        notePane.getChildren().add(deleteIcon);
        notePane.getChildren().add(textArea);

        notesFlowPane.getChildren().add(notePane);
    }

    public void addNewNote(MouseEvent mouseEvent) {
        //if text exist then create new note object and add to array list
        if (!newNoteTextArea.getText().isEmpty()) {
            Note n = new Note();
            n.setText(newNoteTextArea.getText());
            noteArrayList.add(n);
            createNoteTile(n);
            newNoteTextArea.setText("");
        } //if empty then no action
    }

    public int newDeleteAlert() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Confirmation Dialog", ButtonType.OK, ButtonType.CANCEL);
        setDefaultButton(alert, ButtonType.CANCEL); // set default button to cancel
        alert.setHeaderText("Look, do you really want to delete this note?");
        alert.setContentText("Ok to confirm, Cancel to abort");
        Optional<ButtonType> result = alert.showAndWait();
        int decision;
        if (result.get() == ButtonType.OK) {
            decision = 1; // ... user chose OK
        } else {
            decision = 0; // ... user chose CANCEL or closed the dialog
        }
        return decision;
    }

    //method to set default button in alert
    private static Alert setDefaultButton(Alert alert, ButtonType defBtn) {
        DialogPane pane = alert.getDialogPane();
        for (ButtonType t : alert.getButtonTypes())
            ((Button) pane.lookupButton(t)).setDefaultButton(t == defBtn);
        return alert;
    }

    public void returnToMain(MouseEvent mouseEvent) {
        URL resource = getClass().getClassLoader().getResource("main.fxml");
        if (resource != null) {
            try {
                Pane mainPane = FXMLLoader.load(resource);
                Stage stage = (Stage) notesPane.getScene().getWindow();
                stage.setScene(new Scene(mainPane, 1040, 1000));
                stage.show();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


}
