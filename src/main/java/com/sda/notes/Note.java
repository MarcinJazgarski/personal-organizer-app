package com.sda.notes;

import java.io.Serializable;

public class Note implements Serializable {

    private String text;

    public Note() {
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
