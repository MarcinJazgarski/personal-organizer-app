package com.sda.calories;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Properties;

public class AbsorbedCaloriesDTO {


    public void saveToCaloriesPropertiesFile(Double calories) {
        Properties properties = new Properties();
        try {
            FileWriter fw = new FileWriter("saves/calories.properties");
            properties.setProperty(CaloriesPropertiesParameters.CALORIESABSORBED.name(), String.valueOf(calories));
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            String dateAsString = (LocalDateTime.now()).format(dtf);
            properties.setProperty(CaloriesPropertiesParameters.DATEOFUPDATE.name(), dateAsString);
            properties.store(fw, "");
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public Double loadFromCaloriesPropertiesFile() {
        Double absorbedCaloriesFromFile = null;
        Properties properties = new Properties();
        try {
            FileReader fr = new FileReader("saves/calories.properties");
            properties.load(fr);
            String prop = properties.getProperty(CaloriesPropertiesParameters.CALORIESABSORBED.name());
            if (prop != null)
                absorbedCaloriesFromFile = Double.valueOf(prop);
            String dt = properties.getProperty((CaloriesPropertiesParameters.DATEOFUPDATE.name()));
                fr.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return absorbedCaloriesFromFile;
    }

    public String loadDateOfUpdateFromPropertiesFile() {
        String dateOfUpdateFromFile = null;
        Properties properties = new Properties();
        try {
            FileReader fr = new FileReader("saves/calories.properties");
            properties.load(fr);
            String dt = properties.getProperty((CaloriesPropertiesParameters.DATEOFUPDATE.name()));
            if (dt != null)
                dateOfUpdateFromFile = dt;
            fr.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return dateOfUpdateFromFile;
    }


}
