package com.sda.weather;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.Charset;

public class WeatherLoader {

    private Logger logger = LogManager.getLogger(WeatherLoader.class);

    public WeatherDTO loadWeather(String location) {
        WeatherDTO weatherDTO = new WeatherDTO(); //create empty WeatherDTO object (Object should have all fields set that are required by the FXML from the API data)
        String uRL1 = "https://www.metaweather.com/api/location/search/?query=" + location;
        String uRL2 = loadJSONString(uRL1);

        JSONArray jsonArray = new JSONArray(uRL2);
        if (jsonArray.isNull(0)){
            logger.error("Connection problem or location not found in API portal");
            weatherDTO.setCity("(Location not found)");
            weatherDTO.setTheTemp(0);
            weatherDTO.setMinTemp(0);
            weatherDTO.setMaxTemp(0);
            weatherDTO.setWeatherState(WeatherStates.SUN);
            return weatherDTO;
        }
        JSONObject jsonObject = jsonArray.getJSONObject(0);
        String woeid = jsonObject.get("woeid").toString(); //get woeid id (returned by the api as a JSONobject inside an array)
        String city = jsonObject.get("title").toString();
        weatherDTO.setCity(city);

        //load weather data from api
        JSONObject weatherJSON = new JSONObject(loadJSONString("https://www.metaweather.com/api/location/" + woeid + "/"));
        JSONArray consolidatedWeather = weatherJSON.getJSONArray("consolidated_weather"); //consolidated_weather is an array with 6 JSONobjects with weather data for 6 days
        JSONObject weatherToday = consolidatedWeather.getJSONObject(0);
        logger.info("weatherToday: " + weatherToday);

        //weather fields that could be useful in the weather app
        String weatherCreateDate = weatherToday.getString("created");  //field not used in the FXML/app currently
        int maxTemp = weatherToday.getInt("max_temp");
        weatherDTO.setMaxTemp(maxTemp);
        int minTemp = weatherToday.getInt("min_temp");
        weatherDTO.setMinTemp(minTemp);
        int theTemp = weatherToday.getInt("the_temp");
        weatherDTO.setTheTemp(theTemp);
        int airPressure = weatherToday.getInt("air_pressure");  //field not used in the FXML/app currently
        String weatherState = weatherToday.getString("weather_state_name");
        switch (weatherState) {
            case "Thunderstorm":
                weatherDTO.setWeatherState(WeatherStates.STORM);
                break;
            case "Snow":
                weatherDTO.setWeatherState(WeatherStates.SNOW);
                break;
            case "Sleet":
            case "Hail":
            case "Heavy Rain":
            case "Light Rain":
            case "Showers":
                weatherDTO.setWeatherState(WeatherStates.RAIN);
                break;
            case "Light Cloud":
                weatherDTO.setWeatherState(WeatherStates.LIGHTCLOUDS);
                break;
            case "Heavy Cloud":
                weatherDTO.setWeatherState(WeatherStates.HEAVYCLOUDS);
                break;
            default:
                weatherDTO.setWeatherState(WeatherStates.SUN);
        }
        // return WeatherDTO object (Object should have all fields set that are required by the FXML from the API data)
        return weatherDTO;
    }


    private String loadJSONString(String url) {
        // try url input example:   https://www.metaweather.com/api/location/search/?query=Warsaw
        // returns example:         [{"title":"Warsaw","location_type":"City","woeid":523920,"latt_long":"52.235352,21.009390"}]

        StringBuilder jsonText = new StringBuilder();
        try (InputStream is = new URL(url).openStream()) {
            BufferedReader br = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            br.lines().forEach(jsonText::append);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return jsonText.toString();
    }

    public Logger getLogger() {
        return logger;
    }
}

