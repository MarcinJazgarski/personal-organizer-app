package com.sda.weather;

public enum WeatherStates {
    SUN, STORM, SNOW, RAIN, HEAVYCLOUDS, LIGHTCLOUDS
}
