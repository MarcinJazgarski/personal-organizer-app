package com.sda.weather;

import java.time.LocalDateTime;

public class WeatherDTO {

    private WeatherStates weatherState;
    private Integer maxTemp;
    private Integer minTemp;
    private Integer theTemp;
    private String city;
    private LocalDateTime timeStampLastWeatherDTOObjectUpdate;

    @Override
    public String toString() {
        return "WeatherDTO{" +
                "weatherState=" + weatherState +
                ", maxTemp=" + maxTemp +
                ", minTemp=" + minTemp +
                ", theTemp=" + theTemp +
                ", city='" + city + '\'' +
                '}';
    }

    public WeatherStates getWeatherState() {
        return weatherState;
    }

    public void setWeatherState(WeatherStates weatherState) {
        this.weatherState = weatherState;
    }

    public Integer getMaxTemp() {
        return maxTemp;
    }

    public void setMaxTemp(Integer maxTemp) {
        this.maxTemp = maxTemp;
    }

    public Integer getMinTemp() {
        return minTemp;
    }

    public void setMinTemp(Integer minTemp) {
        this.minTemp = minTemp;
    }

    public Integer getTheTemp() {
        return theTemp;
    }

    public void setTheTemp(Integer theTemp) {
        this.theTemp = theTemp;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public LocalDateTime getTimeStampLastWeatherDTOObjectUpdate() {
        return timeStampLastWeatherDTOObjectUpdate;
    }

    public void setTimeStampLastWeatherDTOObjectUpdate(LocalDateTime timeStampLastWeatherDTOObjectUpdate) {
        this.timeStampLastWeatherDTOObjectUpdate = timeStampLastWeatherDTOObjectUpdate;
    }
}