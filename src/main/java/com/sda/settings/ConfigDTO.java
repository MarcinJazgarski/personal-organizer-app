package com.sda.settings;

public class ConfigDTO {

    private String userName;
    private String localization;
    private double caloriesUserLimit;

    public ConfigDTO(String userName, String localization, double caloriesUserLimit) {
        this.userName = userName;
        this.localization = localization;
        this.caloriesUserLimit = caloriesUserLimit;
    }

    public String getUserName() {
        return userName;
    }

    public String getLocalization() {
        return localization;
    }

    public double getCaloriesUserLimit() {
        return caloriesUserLimit;
    }
}
