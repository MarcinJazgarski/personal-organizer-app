package com.sda.settings;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class SettingsController implements Initializable {
    private Logger logger = LogManager.getLogger(SettingsController.class);

    @FXML
    TextField userName;

    @FXML
    TextField localization;

    @FXML
    Parent settingsPane;

    @FXML
    Slider caloriesSlider;

    @FXML
    Label caloriesLabel;


    @Override
    public void initialize(URL location, ResourceBundle resourceBundle) {
        this.caloriesSlider.valueProperty().addListener((observable, oldValue, newValue) -> caloriesLabel.setText(String.valueOf(newValue.intValue())));

        ConfigDTOoperations configDTOoperations = new ConfigDTOoperations();
        userName.setText(configDTOoperations.loadConfigProperties().getUserName());
        localization.setText(configDTOoperations.loadConfigProperties().getLocalization());
        caloriesSlider.setValue(configDTOoperations.loadConfigProperties().getCaloriesUserLimit());
    }

    public void loadMain(MouseEvent mouseEvent) {
        updateProperties();
        URL resource = getClass().getClassLoader().getResource("main.fxml");
        if (resource != null) {
            try {
                Pane mainPane = FXMLLoader.load(resource);
                Stage stage = (Stage) settingsPane.getScene().getWindow();
                stage.setScene(new Scene(mainPane, 1040, 1000));
                stage.show();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void updateProperties() {
        ConfigDTO configDTO = new ConfigDTO(userName.getText(), localization.getText(), caloriesSlider.getValue());
        ConfigDTOoperations configDTOoperations = new ConfigDTOoperations();
        configDTOoperations.saveConfigDTOproperties(configDTO);
    }


}
