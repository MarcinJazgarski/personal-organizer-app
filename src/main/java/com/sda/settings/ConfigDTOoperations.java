package com.sda.settings;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

public class ConfigDTOoperations {

    private static final String CONFIGFILE = "saves/configDTO.properties";


    public void saveConfigDTOproperties(ConfigDTO configDTO) {
        Properties properties = new Properties();
        try {
            FileWriter fw = new FileWriter(CONFIGFILE);
            properties.setProperty(ConfigDTOpropertiesEnum.USERNAME.name(), configDTO.getUserName());
            properties.setProperty(ConfigDTOpropertiesEnum.LOCALIZATION.name(), configDTO.getLocalization());
            properties.setProperty(ConfigDTOpropertiesEnum.CALORIESUSERLIMIT.name(), String.valueOf(configDTO.getCaloriesUserLimit()));
            properties.store(fw, "");
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ConfigDTO loadConfigProperties() {
        String userName = null;
        String localization = null;
        double caloriesUserLimit = 0;

        Properties properties = new Properties();
        try {
            FileReader fr = new FileReader(CONFIGFILE);
            properties.load(fr);
            userName = properties.getProperty(ConfigDTOpropertiesEnum.USERNAME.name());
            localization = properties.getProperty(ConfigDTOpropertiesEnum.LOCALIZATION.name());
            caloriesUserLimit = Double.valueOf(properties.getProperty(ConfigDTOpropertiesEnum.CALORIESUSERLIMIT.name()));
            fr.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ConfigDTO(userName, localization, caloriesUserLimit);
    }




}
